<?php
declare(strict_types=1);

namespace DreamCat\ObjectOrmTest\DemoPojo;

use DreamCat\ObjectOrm\OrmInfo;
use DreamCat\ObjectOrmTest\Helper\Convert;

/**
 * -
 * @author vijay
 */
class Entity
{
    public int $id;
    public string $val;
    #[OrmInfo(Convert::class)]
    public string $alias;
}

# end of file
