<?php
declare(strict_types=1);

namespace DreamCat\ObjectOrmTest\Cases;

use DreamCat\ObjectOrm\ObjectOrm;
use DreamCat\ObjectOrmTest\DemoPojo\Entity;
use DreamCat\ObjectOrmTest\Helper\Container;
use PHPUnit\Framework\TestCase;
use RuntimeException;

/**
 * -
 * @author vijay
 */
class OrmTest extends TestCase
{
    public function testNormal()
    {
        $container = new Container();
        $orm = new ObjectOrm($container);
        self::assertEquals(spl_object_id($container), spl_object_id($orm->getContainer()));

        $data = [
            "id" => 3,
            "val" => uniqid(),
            "alias" => uniqid(),
        ];
        $obj = $orm->toModel($data, Entity::class);
        $expect = new Entity();
        $expect->id = $data["id"];
        $expect->val = $data["val"];
        $expect->alias = "prefix---{$data["alias"]}";
        self::assertEquals($expect, $obj);

        self::assertEquals($data, $orm->toDatabaseArray($expect));

        self::assertEquals($expect, $orm->toModel((object)$data, Entity::class));
    }

    public function testError()
    {
        $this->expectException(RuntimeException::class);
        $this->expectErrorMessage("指定的模型类 int 不存在");
        (new ObjectOrm(new Container()))->toModel([], "int");
    }
}

# end of file
