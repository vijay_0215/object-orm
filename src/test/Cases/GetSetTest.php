<?php
declare(strict_types=1);

namespace DreamCat\ObjectOrmTest\Cases;

use DreamCat\ObjectOrm\Array2Class;
use DreamCat\ObjectOrm\Class2Array;
use DreamCat\ObjectOrm\ObjectOrm;
use DreamCat\ObjectOrm\Utils;
use DreamCat\ObjectOrmTest\Helper\Container;
use DreamCat\ObjectOrmTest\Helper\Convert;
use PHPUnit\Framework\TestCase;

/**
 * -
 * @author vijay
 */
class GetSetTest extends TestCase
{
    public function testGetterSetter()
    {
        $container = new Container();
        $a2c = new Array2Class($container);
        $a2c->setContainer($container);
        self::assertEquals(spl_object_id($container), spl_object_id($a2c->getContainer()));
        $c2a = new Class2Array($container);
        $c2a->setContainer($container);
        self::assertEquals(spl_object_id($container), spl_object_id($c2a->getContainer()));

        $orm = new ObjectOrm($container);
        $orm->setContainer($container);
        self::assertEquals(spl_object_id($container), spl_object_id($orm->getContainer()));
        $orm->setArray2Class($a2c);
        $orm->setJsonValueFixer($c2a);
        self::assertEquals(spl_object_id($a2c), spl_object_id($orm->getArray2Class()));
        self::assertEquals(spl_object_id($c2a), spl_object_id($orm->getJsonValueFixer()));
    }

    public function testUtilsContainer()
    {
        $container = new Container();
        self::assertNull(Utils::getObjectFromContainer($container, "int"));

        self::assertNull(Utils::getObjectFromContainer($container, Convert::class, "abc"));
    }
}

# end of file
