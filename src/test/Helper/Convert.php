<?php
declare(strict_types=1);

namespace DreamCat\ObjectOrmTest\Helper;

use DreamCat\ObjectOrm\DataConvertInterface;

/**
 * -
 * @author vijay
 */
class Convert implements DataConvertInterface
{
    /**
     * @inheritDoc
     */
    public function db2model(float|int|string|null $dbRecord): mixed
    {
        return "prefix---{$dbRecord}";
    }

    /**
     * @inheritDoc
     */
    public function model2db(mixed $data): string|int|float|null
    {
        return substr($data, 9);
    }
}

# end of file
