<?php
declare(strict_types=1);

namespace DreamCat\ObjectOrmTest\Helper;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

/**
 * -
 * @author vijay
 */
class Container implements ContainerInterface
{
    private array $cache;
    public function __construct() {
        $this->cache = [
            Convert::class => new Convert(),
        ];
    }
    /**
     * @inheritDoc
     */
    public function get(string $id)
    {
        return $this->cache[$id] ?? null;
    }

    /**
     * @inheritDoc
     */
    public function has(string $id): bool
    {
        return isset($this->cache[$id]);
    }
}

# end of file
