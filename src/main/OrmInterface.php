<?php

declare(strict_types=1);

namespace DreamCat\ObjectOrm;

use stdClass;

/**
 * orm转换器接口
 * @author vijay
 */
interface OrmInterface
{
    /**
     * 将模型对象转为数组，准备存为数据库
     * @param object $modelObj 模型对象
     * @return array
     */
    public function toDatabaseArray(object $modelObj): array;

    /**
     * 将数据库取出的数据改为模型对象
     * @param array|stdClass $model 从数据库取出的数据
     * @psalm-param class-string<T> $modelType 模型对象类名
     * @psalm-return T 模型对象
     * @template T
     */
    public function toModel(array|stdClass $model, string $modelType): object;
}

# end of file
