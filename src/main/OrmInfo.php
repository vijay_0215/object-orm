<?php

declare(strict_types=1);

namespace DreamCat\ObjectOrm;

use Attribute;
use JetBrains\PhpStorm\Immutable;

/**
 * ORM信息注解
 * @author vijay
 */
#[Attribute(Attribute::TARGET_PROPERTY)]
#[Immutable]
class OrmInfo
{
    public function __construct(private ?string $convert = null)
    {
    }

    /**
     * @return DataConvertInterface|string|null
     */
    public function getConvert(): DataConvertInterface|string|null
    {
        return $this->convert;
    }
}

# end of file
