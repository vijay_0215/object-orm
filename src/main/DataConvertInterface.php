<?php

declare(strict_types=1);

namespace DreamCat\ObjectOrm;

/**
 * 数据转换接口
 * @author vijay
 * @template T
 * @template R
 */
interface DataConvertInterface
{
    /**
     * 数据库的数据转为模型数值
     * @psalm-param R $dbRecord 从数据库取出的数据
     * @psalm-return T 转换后的数据
     */
    public function db2model(string|int|float|null $dbRecord): mixed;

    /**
     * 模型数据转为数据库的数值
     * @psalm-param T $data 模型数据
     * @psalm-return R
     */
    public function model2db(mixed $data): string|int|float|null;
}

# end of file
