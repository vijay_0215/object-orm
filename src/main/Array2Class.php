<?php
declare(strict_types=1);

namespace DreamCat\ObjectOrm;

use DreamCat\Array2Class\Array2ClassConverter;
use Dreamcat\PropertyAnalysis\Pojo\PropertyResult;
use Psr\Container\ContainerInterface;

/**
 * -
 * @author vijay
 */
class Array2Class extends Array2ClassConverter
{
    /**
     * @param ContainerInterface $container 容器
     */
    public function __construct(private ContainerInterface $container)
    {
    }

    /**
     * @return ContainerInterface 容器
     */
    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    /**
     * @param ContainerInterface $container 容器
     */
    public function setContainer(ContainerInterface $container): void
    {
        $this->container = $container;
    }

    protected function setPropertyValue(PropertyResult $propertyResult, mixed $value, object $object): void
    {
        $info = Utils::getAttrObj($propertyResult->getProperty(), OrmInfo::class);
        if ($info) {
            $convert = Utils::getObjectFromContainer(
                $this->getContainer(),
                $info->getConvert(),
                DataConvertInterface::class
            );
            if ($convert) {
                $value = $convert->db2model($value);
            }
        }
        parent::setPropertyValue($propertyResult, $value, $object);
    }
}

# end of file
